import java.util.ArrayDeque;

public class SimpleBuffer implements IBuffer<Task>{
    private ArrayDeque<Task> tasks;

    public SimpleBuffer(int capacity){
        this.tasks = new ArrayDeque<>(capacity);
    }

    public void add(Task task) {
        tasks.offerLast(task);
    }

    public Task get() {
        return tasks.pollFirst();
    }

    public int size() {
        return tasks.size();
    }

    public void clear() {
        tasks.clear();
    }

    public boolean isEmpty() {
        return tasks.isEmpty();
    }
}
