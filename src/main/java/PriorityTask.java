import java.util.Objects;

public class PriorityTask extends Task implements ITask, Comparable<PriorityTask> {
    int priority;

    public PriorityTask(int priority, Integer ... data){
        super(data);
        this.priority = priority;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        PriorityTask that = (PriorityTask) o;
        return priority == that.priority;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), priority);
    }

    @Override
    public int compareTo(PriorityTask priorityTask) {
        return Integer.compare(this.priority, priorityTask.priority);
    }
}
