import java.util.Arrays;
import java.util.Collection;

public class Task implements ITask {
    private int[] data;

    public Task(Integer ... data){
        this.data = new int[data.length];
        for(int i = 0; i < this.data.length; i++){
            this.data[i] = data[i];
        }
    }

    public int[] getData() {
        return data;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Task task = (Task) o;
        return Arrays.equals(data, task.data);
    }

    @Override
    public int hashCode() {
        return Arrays.hashCode(data);
    }
}
