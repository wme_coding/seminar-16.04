import java.util.ArrayList;
import java.util.List;

public class SimpleTaskGenerator implements ITaskGenerator {
    private SimpleBuffer simpleBuffer;
    private int startValue;
    private int amount;

    public SimpleTaskGenerator(SimpleBuffer simpleBuffer, int startValue, int amount) {
        this.simpleBuffer = simpleBuffer;
        this.startValue = startValue;
        this.amount = amount;
    }

    public SimpleTaskGenerator(SimpleBuffer simpleBuffer) {
        this.simpleBuffer = simpleBuffer;
    }

    public void generate() {
        List<Integer> temp = new ArrayList<Integer>();
        for(int i = 0; i < amount; i++){
            temp.add(startValue + i);
        }
        Task task = new Task(temp.toArray(Integer[]::new));
        simpleBuffer.add(task);
    }

    SimpleTaskGenerator withStartValue(int startValue){
        this.startValue = startValue;
        return this;
    }

    SimpleTaskGenerator withAmount(int amount){
        this.amount = amount;
        return this;
    }
}
