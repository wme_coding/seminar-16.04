public interface IBuffer <T extends Task>{

    void add(T task);

    T get();

    int size();

    void clear();

    boolean isEmpty();
}
