import java.util.Arrays;

public class SimpleTaskProcessor implements ITaskProcessor {
    SimpleBuffer simpleBuffer;

    public SimpleTaskProcessor(SimpleBuffer simpleBuffer) {
        this.simpleBuffer = simpleBuffer;
    }


    public Integer process() {
        if (simpleBuffer.isEmpty()){
            return null;
        }
        return Arrays.stream(simpleBuffer.get().getData()).sum();
    }
}
