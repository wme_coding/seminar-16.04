import java.util.PriorityQueue;

public class SimpleBufferWithPriority implements IBuffer<PriorityTask>{
    private PriorityQueue<PriorityTask> priorityQueue;
    //В общем я что-то не так делаю, потому что оно не сортирует элементы по приоритету

    public SimpleBufferWithPriority(int capacity) {
        priorityQueue = new PriorityQueue<>(capacity, PriorityTask::compareTo);
    }

    @Override
    public void add(PriorityTask task) {
        priorityQueue.offer(task);
    }

    @Override
    public PriorityTask get() {
        return priorityQueue.poll();
    }

    @Override
    public int size() {
        return priorityQueue.size();
    }

    @Override
    public void clear() {
        priorityQueue.clear();
    }

    @Override
    public boolean isEmpty() {
        return priorityQueue.isEmpty();
    }
}
