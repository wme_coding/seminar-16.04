import java.util.ArrayList;
import java.util.List;

public class SimpleTaskGeneratorWithPriority implements ITaskGenerator{
    private SimpleBufferWithPriority simpleBufferWithPriority;
    private int startValue;
    private int amount;
    private int priority;

    public SimpleTaskGeneratorWithPriority(SimpleBufferWithPriority simpleBufferWithPriority, int startValue, int amount, int priority) {
        this.simpleBufferWithPriority = simpleBufferWithPriority;
        this.startValue = startValue;
        this.amount = amount;
        this.priority = priority;
    }

    public SimpleTaskGeneratorWithPriority(SimpleBufferWithPriority simpleBufferWithPriority) {
        this.simpleBufferWithPriority = simpleBufferWithPriority;
    }

    public void generate() {
        List<Integer> temp = new ArrayList<Integer>();
        for(int i = 0; i < amount; i++){
            temp.add(startValue + i);
        }
        PriorityTask task = new PriorityTask(priority, temp.toArray(Integer[]::new));
        //Не понял, что вообще написал в параметры toArray
        //Просто сделать апкаст не получилось, и предложило вот это
        simpleBufferWithPriority.add(task);
    }

    SimpleTaskGeneratorWithPriority withStartValue(int startValue){
        this.startValue = startValue;
        return this;
    }

    SimpleTaskGeneratorWithPriority withAmount(int amount){
        this.amount = amount;
        return this;
    }

    SimpleTaskGeneratorWithPriority withPriority(int priority){
        this.priority = priority;
        return this;
    }
}
