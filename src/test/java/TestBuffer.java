import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

public class TestBuffer {

    @Test
    public void testBufferReturnedTasks(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(3);

        Task firstTask = new Task(1, 1, 1);
        Task secondTask = new Task(2, 2, 2);
        Task thirdTask = new Task(3, 3 ,3);

        simpleBuffer.add(firstTask);
        simpleBuffer.add(secondTask);
        simpleBuffer.add(thirdTask);

        Assert.assertEquals(firstTask, simpleBuffer.get());
        Assert.assertEquals(secondTask, simpleBuffer.get());
        Assert.assertEquals(thirdTask, simpleBuffer.get());
    }

    @Ignore
    @Test(expected = Exception.class)
    public void testCapacity(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(1);

        Task firstTask = new Task(1, 1, 1);
        Task secondTask = new Task(2, 2, 2);
        Task thirdTask = new Task(3, 3 ,3);

        simpleBuffer.add(firstTask);
        simpleBuffer.add(secondTask);
        simpleBuffer.add(thirdTask);
    }

    @Ignore
    @Test
    public void testBufferWithPriorityReturnedTasks(){
        SimpleBufferWithPriority simpleBufferWithPriority = new SimpleBufferWithPriority(3);

        PriorityTask firstTask = new PriorityTask(64, 1, 1, 1);
        PriorityTask secondTask = new PriorityTask(88, 2, 2);
        PriorityTask thirdTask = new PriorityTask(23, 3, 3);

        simpleBufferWithPriority.add(firstTask);
        simpleBufferWithPriority.add(secondTask);
        simpleBufferWithPriority.add(thirdTask);

        Assert.assertEquals(secondTask, simpleBufferWithPriority.get());
        Assert.assertEquals(firstTask, simpleBufferWithPriority.get());
        Assert.assertEquals(thirdTask, simpleBufferWithPriority.get());
    }
}
