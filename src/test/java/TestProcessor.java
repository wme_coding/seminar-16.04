import org.junit.Assert;
import org.junit.Test;

public class TestProcessor {

    @Test
    public void testSimpleTaskProcessor(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(5);
        SimpleTaskGenerator simpleTaskGenerator = new SimpleTaskGenerator(simpleBuffer);
        SimpleTaskProcessor simpleTaskProcessor = new SimpleTaskProcessor(simpleBuffer);

        simpleTaskGenerator.withStartValue(1).withAmount(4).generate();
        simpleTaskGenerator.withStartValue(2).withAmount(5).generate();
        simpleTaskGenerator.withStartValue(0).withAmount(3).generate();

        Assert.assertEquals(Integer.valueOf(10), simpleTaskProcessor.process());
        Assert.assertEquals(Integer.valueOf(20), simpleTaskProcessor.process());
        Assert.assertEquals(Integer.valueOf(3), simpleTaskProcessor.process());
        Assert.assertNull(simpleTaskProcessor.process());
    }
}
