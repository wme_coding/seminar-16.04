import org.junit.Assert;
import org.junit.Test;

public class TestGenerator {

    @Test
    public void testSimpleGenerator(){
        SimpleBuffer simpleBuffer = new SimpleBuffer(5);
        SimpleTaskGenerator simpleTaskGenerator = new SimpleTaskGenerator(simpleBuffer);

        simpleTaskGenerator.withStartValue(1).withAmount(4).generate();
        simpleTaskGenerator.withStartValue(2).withAmount(5).generate();
        simpleTaskGenerator.withStartValue(0).withAmount(3).generate();

        Task expectedTask1 = new Task(1, 2, 3 , 4);
        Task expectedTask2 = new Task( 2, 3 , 4, 5, 6);
        Task expectedTask3 = new Task(0, 1, 2);

        Assert.assertEquals(expectedTask1, simpleBuffer.get());
        Assert.assertEquals(expectedTask2, simpleBuffer.get());
        Assert.assertEquals(expectedTask3, simpleBuffer.get());
    }
}
